const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");

router.post("/checkEmail", (req, res) => {
    userController.checkEmailExist(req.body).then(resultFromController => 
        res.send(resultFromController))
});

router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => 
        res.send(resultFromController))
});

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => 
        res.send(resultFromController))
})

router.post("/details", (req, res) => {
    userController.checkDetails(req.body).then(resultFromController => 
        res.send(resultFromController))
})

module.exports = router;