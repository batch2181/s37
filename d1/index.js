// D E P E N D E N C I E S
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require('./routes/userRoutes');


const app = express();
const port = 4000;

// M I D D L E W A R E S

app.use(cors()); // cross origin resource sharing
app.use(express.json()); // to read json jobjects
app.use(express.urlencoded({extended: true})); // to read forms
app.use("/users", userRoutes)


// M O N G O O S E   C O N N E C T I O N

mongoose.connect("mongodb+srv://admin:admin123@b218-course-booking.vejokfg.mongodb.net/test", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
const db = mongoose.connection
db.on('error', () => console.error('Connection Error.'))
db.once('open', () => console.log('Connected to MongoDB!'))



// L I S T E N
app.listen(port, () => console.log(`API is now online at port: ${port}`));